import { Mark } from './mark';

export class Student {
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    city: string;
    state: string;
    dateOfBirth: any;
    genderID: number;
    marks: Mark[];
}