export class Mark {
    id: number;    
    markValue: string;
    courseID: number;
    studentID: number;
}