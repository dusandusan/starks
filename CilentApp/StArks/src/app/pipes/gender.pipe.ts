import { Pipe, PipeTransform } from '@angular/core';
import { Gender } from "../models/gender";

@Pipe({ name: 'gender' })
export class GenderPipe implements PipeTransform {
    transform(value: number, args: Gender[]): any {
        if (!(value && args)) return value;

        return args.find(x => x.id == value).name;
    }
}