import { Component, OnInit, TemplateRef } from '@angular/core';
import { CoursesService } from '../services/courses.service';
import {Router, ActivatedRoute} from "@angular/router";
import { Course } from '../models/course';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  modalRef: BsModalRef;
  public courses: Course[]

  public model: Course;
  title: string;
  modal = true;
  submitType: string = 'Save';

  alerts: any[] = [];

  constructor(
    private courseService: CoursesService,
    private modalService: BsModalService
    ) { }

  ngOnInit() {
      this.courseService.getAllCourses().subscribe(
        res => this.courses = res,
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error,
            timeout: 10000
          });
        });

  }

  onNew(template: TemplateRef<any>) {
    this.model = new Course();
    this.title = "Add new course";
    this.submitType = 'Save';
    this.modalRef = this.modalService.show(template);
    
  }

  edit(id: number, template: TemplateRef<any>) {
    this.courseService.getCourse(id)
      .subscribe(
        res => {
          this.model = res;
          this.modalRef = this.modalService.show(template);
          this.title = "Edit course";
          this.submitType = 'Update';
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.message,
            timeout: 10000
          });
        })
  }

  onSave() {
    if (this.submitType === 'Save') {
      this.courseService.saveCourse(this.model)
        .subscribe(res => {
          let newCourse: Course = res;
            this.courseService.getAllCourses().subscribe(res => {
              this.courses = res;
              this.modalRef.hide();
              this.alerts.push({
                type: 'info',
                msg: `You have successfuly added new course, name: ${newCourse.name}, code: ${newCourse.code} (added: ${new Date().toLocaleTimeString()})`,
                timeout: 10000
              });
            },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: error.message,
                timeout: 10000
              });
            });}
        );
    } else {
      this.courseService.updateCourse(this.model)
        .subscribe(
          res => {
            let updatedCourse: Course = res;
            this.courseService.getAllCourses().subscribe(res => {
              this.courses = res;              
              this.modalRef.hide();
              this.alerts.push({
                type: 'info',
                msg: `You have successfuly updated course, name: ${updatedCourse.name}, code: ${updatedCourse.code} (updated: ${new Date().toLocaleTimeString()})`,
                timeout: 10000
              });
            })
          },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: error.message,
              timeout: 10000
            });
          });
    }
  }

  onDelete(id: number) {    
    this.courseService.deleteCourse(id)
      .subscribe(res => {
        let deletedCourse: Course = res;
        this.courseService.getAllCourses().subscribe(res => {
          this.courses = res;
          this.alerts.push({
            type: 'info',
            msg: `You have successfuly deleted course, name: ${deletedCourse.name}, code: ${deletedCourse.code} (deleted: ${new Date().toLocaleTimeString()})`,
            timeout: 10000
          });
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.message,
            timeout: 10000
          });
        });
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: error.message,
          timeout: 10000
        });
      });
  }

  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}


