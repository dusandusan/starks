import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component'
import { StudentsComponent } from './students/students.component';
import { CoursesComponent } from './courses/courses.component';
import { StudentsService} from './services/students.service';
import { CoursesService} from './services/courses.service';
import { GenderPipe } from './pipes/gender.pipe';
import { from } from 'rxjs';
import { ModalModule, BsDatepickerModule, AlertModule } from 'ngx-bootstrap';
import { MarksComponent } from './marks/marks.component';
import { StarksComponent } from './starks/starks.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    StudentsComponent,
    CoursesComponent, 
    GenderPipe, MarksComponent, StarksComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AlertModule.forRoot(),
    FormsModule
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { 

}
