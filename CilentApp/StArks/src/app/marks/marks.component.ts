import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { CoursesService } from '../services/courses.service';
import { StudentsService } from '../services/students.service';
import { MarksService } from '../services/marks.service';
import { Course } from '../models/course';
import { Student } from '../models/student';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Mark } from '../models/mark';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {

  modalRef: BsModalRef;

  public students: Student[];

  public courses: Course[];

  public model: Mark;

  private submitType: string = 'Save';

  private title: string;

  alerts: any[] = [];

  @ViewChild('template') template: TemplateRef<any>;

  constructor(
    private courseService: CoursesService,
    private studentService: StudentsService,
    private markService: MarksService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.studentService.getAllStudents().subscribe(
      res => this.students = res,
      error => {
        this.alerts.push({
          type: 'danger',
          msg: error.message,
          timeout: 10000
        });
      });
    this.courseService.getAllCourses().subscribe(
      res => this.courses = res,
      error => {
        this.alerts.push({
          type: 'danger',
          msg: error.message,
          timeout: 10000
        });
      });
  }

  newMark($event) {
    this.model = new Mark();
    this.model.studentID = $event.studentID;
    this.model.courseID = $event.courseID;
    this.modalRef = this.modalService.show(this.template);
    this.title = "New mark";
    this.submitType = 'Save';
  }

  editMark($event: number) {
    let id = $event;
    this.markService.getMark(id)
      .subscribe(
        res => {
          this.model = res;
          this.modalRef = this.modalService.show(this.template);
          this.title = "Edit mark";
          this.submitType = 'Update';
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.message,
            timeout: 10000
          });
        }
      )
  }

  onSave() {
    if (this.submitType === 'Save') {
      this.markService.saveMark(this.model)
        .subscribe(res => {
          let newMark: Mark = res;
          this.studentService.getAllStudents().subscribe(res => {
            this.students = res;
            let student = this.findStudent(newMark);
            let course = this.findCourse(newMark);
            this.modalRef.hide();
            this.alerts.push({
              type: 'info',
              msg: `You have successfuly added mark, ${newMark.markValue} for ${student.firstName} ${student.lastName} for ${course.name}(updated: ${new Date().toLocaleTimeString()})`,
              timeout: 10000
            });
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: error.message,
                timeout: 10000
              });
            })
        },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: error.message,
              timeout: 10000
            });
          }
        );
    } else {
      this.markService.updateMark(this.model)
        .subscribe(
          res => {
            let updatedMark: Mark = res;
            this.studentService.getAllStudents().subscribe(res => {
              this.students = res;
              let student = this.findStudent(updatedMark);
              let course = this.findCourse(updatedMark);
              this.modalRef.hide();
              this.alerts.push({
                type: 'info',
                msg: `You have successfuly updated ${course.name}'s mark to ${updatedMark.markValue} for ${student.firstName} ${student.lastName}(updated: ${new Date().toLocaleTimeString()})`,
                timeout: 10000
              });
            },
              error => {
                this.alerts.push({
                  type: 'danger',
                  msg: error.message,
                  timeout: 10000
                });
              })
          },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: error.message,
              timeout: 10000
            });
          }
        );
    }
  }

  removeMark($event) {
    this.markService.deleteMark($event)
      .subscribe(res => {
        let deletedMark: Mark = res;
        let student = this.findStudent(deletedMark);
        let course = this.findCourse(deletedMark);
        this.studentService.getAllStudents().subscribe(res => {
          this.students = res;
          this.alerts.push({
            type: 'info',
            msg: `You have successfuly deleted ${course.name}'s mark to ${deletedMark.markValue} for ${student.firstName} ${student.lastName}(updated: ${new Date().toLocaleTimeString()})`,
            timeout: 10000
          });
        });
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.message,
            timeout: 10000
          });
        });
  }

  findStudent(mark: Mark): Student {
    return this.students.find(x => x.id == mark.studentID)
  }

  findCourse(mark: Mark): Course {
    return this.courses.find(x => x.id == mark.courseID)
  }


  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
