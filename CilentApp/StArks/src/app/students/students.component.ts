import { Component, OnInit, TemplateRef } from '@angular/core';
import { StudentsService } from '../services/students.service';
import { Student } from '../models/student';
import { Gender } from '../models/gender';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { CollapseModule } from 'ngx-bootstrap';
import { DATE } from 'ngx-bootstrap/chronos/units/constants';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  modalRef: BsModalRef;
  public students: Student[];

  public genders: Gender[];

  public model: Student;

  private submitType: string = 'Save';
  
  private maxDate: Date;
  private title: string;

  alerts: any[] = [];

  constructor(
    private studentService: StudentsService,    
    private modalService: BsModalService) { 
    
    this.maxDate = new Date();
    let year = this.maxDate.getFullYear() - 18;
    this.maxDate.setFullYear(year);
  }

  ngOnInit() {

    this.studentService.getAllStudents().subscribe(
      res => this.students = res,
      error => {
        this.alerts.push({
          type: 'danger',
          msg: error.message,
          timeout: 10000
        });
      });
    this.studentService.getAllGenders().subscribe(
      res => this.genders = res,
      error => {
        this.alerts.push({
          type: 'danger',
          msg: error.message,
          timeout: 10000
        });
      });
  }

  onNew(template: TemplateRef<any>) {
    this.model = new Student();    
    this.title = "Add new student";
    this.submitType = 'Save';
    this.modalRef = this.modalService.show(template);
  }

  cancel() {
  }

  edit(id: number, template: TemplateRef<any>) {
    this.studentService.getStudent(id)
      .subscribe(
        res => {
          this.model = new Student();
          this.model = res;
          this.model.dateOfBirth = new Date(this.model.dateOfBirth);
          this.modalRef = this.modalService.show(template);
          this.title = "Edit student";
          this.submitType = 'Update';
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error.message,
            timeout: 10000
          });
        });
  }

  onSave() {
    if (this.submitType === 'Save') {
      this.studentService.saveStudent(this.model)
        .subscribe(
          res => {
            let student: Student = res;
            this.studentService.getAllStudents().subscribe(res => {
              this.students = res;              
              this.modalRef.hide();
              this.alerts.push({
                type: 'info',
                msg: `You have successfuly added new student, ${student.firstName} ${student.lastName} (added: ${new Date().toLocaleTimeString()})`,
                timeout: 10000
              });
            },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: error.message,
                timeout: 10000
              });
            })
          },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: error.message,
              timeout: 10000
            });
          });
    } else {
      this.studentService.updateStudent(this.model)
        .subscribe(
          res => {
            let student: Student = res;
            this.studentService.getAllStudents().subscribe(res => {
              this.students = res;              
              this.modalRef.hide();
              this.alerts.push({
                type: 'info',
                msg: `You have successfuly updated student, ${student.firstName} ${student.lastName} (updated: ${new Date().toLocaleTimeString()})`,
                timeout: 10000
              });
            },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: error.message,
                timeout: 10000
              });
            })
          },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: error.message,
              timeout: 10000
            });
          });
    }
  }

  onDelete(id: number) {    
    this.studentService.deleteStudent(id)
      .subscribe(res => {
        let deletedStudent: Student = res;
        this.studentService.getAllStudents().subscribe(res => {
          this.students = res;
          this.alerts.push({
            type: 'info',
            msg: `You have successfuly deleted student, ${deletedStudent.firstName} ${deletedStudent.lastName} (deleted: ${new Date().toLocaleTimeString()})`,
            timeout: 10000
          });});
      },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: error.message,
              timeout: 10000
            });
          });
  }
  
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
