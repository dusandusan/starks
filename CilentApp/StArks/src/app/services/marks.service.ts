import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mark } from '../models/mark';

@Injectable({
  providedIn: 'root'
})
export class MarksService {
  private baseApiUrl = 'http://localhost:59456/api/marks/'

  constructor(private http: HttpClient) { }

  public getAllMarks(): Observable<Mark[]>{    
    return this.http.get<Mark[]>(this.baseApiUrl);
  }

  public getMark(id:number): Observable<Mark>{
    return this.http.get<Mark>(`${this.baseApiUrl}${id}`);
  }

  public saveMark(course: Mark): Observable<Mark> {
    return this.http.post<Mark>(this.baseApiUrl, course);
  }

  public updateMark(course: Mark): Observable<Mark>{
    return this.http.put<Mark>(this.baseApiUrl, course);
  }

  public deleteMark(id:number): Observable<Mark>{
    return this.http.delete<Mark>(`${this.baseApiUrl}${id}`);
  }
}
