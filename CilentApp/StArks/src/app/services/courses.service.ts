import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Course } from '../models/course';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  
  private baseApiUrl = 'http://localhost:59456/api/courses/'

  constructor(private http: HttpClient) { }

  public getAllCourses(): Observable<Course[]>{    
    return this.http.get<Course[]>(this.baseApiUrl);
  }

  public getCourse(id:number): Observable<Course>{
    return this.http.get<Course>(`${this.baseApiUrl}${id}`);
  }

  public saveCourse(course: Course): Observable<Course> {
    return this.http.post<Course>(this.baseApiUrl, course);
  }

  public updateCourse(course: Course): Observable<Course>{
    return this.http.put<Course>(this.baseApiUrl, course);
  }

  public deleteCourse(id:number): Observable<Course>{
    return this.http.delete<Course>(`${this.baseApiUrl}${id}`);
  }
}
