import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../models/student';
import { Gender } from '../models/gender';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  private baseApiUrl = 'http://localhost:59456/api/students/'

  constructor(private http: HttpClient) { }

  public getAllStudents(): Observable<Student[]>{    
    return this.http.get<Student[]>(this.baseApiUrl);
  }

  public getStudent(id:number): Observable<Student>{
    return this.http.get<Student>(`${this.baseApiUrl}${id}`);
  }

  public saveStudent(course: Student): Observable<Student> {
    return this.http.post<Student>(this.baseApiUrl, course);
  }

  public updateStudent(course: Student): Observable<Student>{
    return this.http.put<Student>(this.baseApiUrl, course);
  }

  public deleteStudent(id:number): Observable<Student>{
    return this.http.delete<Student>(`${this.baseApiUrl}${id}`);
  }

  public getAllGenders(): Observable<Gender[]>{    
    return this.http.get<Gender[]>(this.baseApiUrl+ 'GetGenders');
  }
}
