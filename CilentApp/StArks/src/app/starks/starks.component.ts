import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Course } from '../models/course';
import { Student } from '../models/student';
import{ Mark } from '../models/mark'

@Component({
  selector: 'starks',
  templateUrl: './starks.component.html',
  styleUrls: ['./starks.component.css']
})
export class StarksComponent implements OnInit {

  @Input('student') student: Student;
  @Input('courses') courses: Course[];

  @Output() edit = new EventEmitter<number>();

  @Output() new = new EventEmitter<any>();

  @Output() remove = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
    // this.student.marks.find(x => x.courseID == this.course.CourseID)
  }

  hasMark(course: Course): boolean{
    let res = this.student.marks.
    find(x => x.courseID == course.id);
    return res == undefined? false: true;
  }

  getMark(course): Mark{
    return this.student.marks.find(x => x.courseID == course.id);
  }

  editMark(markID: number){
    this.edit.emit(markID);
  }

  newMark(courseId: number){
    this.new.emit({courseID: courseId, studentID: this.student.id})
  }

  removeMark(markID: number){
    this.remove.emit(markID);
  }

}
