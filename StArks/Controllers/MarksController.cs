﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StArks.Data;
using StArks.Data.Entities;
using StArks.ViewModels;

namespace StArks.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarksController : Controller
    {
        private readonly IStArksRepository _repository;
        private readonly IMapper _mapper;

        public MarksController(IStArksRepository repository,
            IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }
        
        [HttpPost]
        public IActionResult Post([FromBody]MarkVM model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                Mark mark = _mapper.Map<MarkVM, Mark>(model);

                _repository.AddEntity(mark);

                if (_repository.SaveAll())
                {
                    return Created($"api/orders/{mark.ID}", mark);
                }

            }
            catch (Exception)
            {
                return BadRequest("Server error");
            }
            return BadRequest("Failed to save mark");
        }

        [HttpGet("{id}")]
        public IActionResult GetMark([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MarkVM mark = _mapper.Map<Mark, MarkVM>(_repository.GetMark(id));

            if (mark == null)
            {
                return NotFound();
            }

            return Ok(mark);
        }



        [HttpPut]
        public IActionResult PutMark([FromBody] MarkVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Mark mark = _mapper.Map<MarkVM, Mark>(model);

            if (!_repository.MarkExist(mark.ID))
            {
                return NotFound();
            }

            _repository.UpdateEntity(mark);
            if (_repository.SaveAll())
            {
                return Ok(mark);
            }

            return NoContent();
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteMark([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_repository.MarkExist(id))
            {
                return NotFound();
            }
            Mark mark = _repository.GetMark(id);

            _repository.DeleteEntity(mark);

            if (_repository.SaveAll())
            {
                return Ok(mark);
            }

            return BadRequest("Mark not deleted");
        }

        private bool MarkExists(int id)
        {
            return _repository.CourseExist(id);
        }
    }
}
