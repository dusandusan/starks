﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StArks.Data;
using StArks.Data.Entities;
using StArks.ViewModels;

namespace StArks.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : Controller
    {
        private readonly IStArksRepository _repository;
        private readonly IMapper _mapper;

        public CoursesController(IStArksRepository repository,
            IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }

        // GET: api/Courses
        [HttpGet]
        public IActionResult GetCourses()
        {
            return Ok(_mapper.Map<IEnumerable<Course>, IEnumerable<CourseVM>>(_repository.GetAllCourses()));
        }

        [HttpPost]
        public IActionResult Post([FromBody]CourseVM model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                Course course = _mapper.Map<CourseVM, Course>(model);

                _repository.AddEntity(course);

                if (_repository.SaveAll())
                {
                    return Created($"api/orders/{course.ID}", course);
                }

            }
            catch (Exception)
            {
                return BadRequest("Server error");
            }
            return BadRequest("Failed to save course");
        }

        [HttpGet("{id}")]
        public IActionResult GetCourse([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CourseVM course = _mapper.Map<Course, CourseVM>(_repository.GetCourse(id));

            if (course == null)
            {
                return NotFound();
            }

            return Ok(course);
        }



        [HttpPut]
        public IActionResult PutCourse([FromBody] CourseVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Course course = _mapper.Map<CourseVM, Course>(model);

            if (!_repository.CourseExist(course.ID))
            {
                return NotFound();
            }

            _repository.UpdateEntity(course);
            if (_repository.SaveAll())
            {
                return Ok(course);
            }

            return NoContent();
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteCourse([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_repository.CourseExist(id))
            {
                return NotFound();
            }
            Course course = _repository.GetCourse(id);

            _repository.DeleteEntity(course);

            if (_repository.SaveAll())
            {
                return Ok(course);
            }

            return BadRequest("Course not deleted");
        }

        private bool CourseExists(int id)
        {
            return _repository.CourseExist(id);
        }
    }
}