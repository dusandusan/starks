﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StArks.Data;
using StArks.Data.Entities;
using StArks.ViewModels;

namespace StArks.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : Controller
    {
        private readonly IStArksRepository _repository;
        private readonly IMapper _mapper;

        public StudentsController(IStArksRepository repository,
            IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetCourses()
        {
            return Ok(_mapper.Map<IEnumerable<Student>, IEnumerable<StudentVM>>(_repository.GetAllStudents()));
        }

        [HttpGet("{id}")]
        public IActionResult GetStudent([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Student s = _repository.GetStudent(id);
            StudentVM student = _mapper.Map<Student, StudentVM>(s);

            int index = student.DateOfBirth.IndexOf(' ');
            student.DateOfBirth = student.DateOfBirth.Substring(0, index);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        // POST: api/Students
        [HttpPost]
        public IActionResult Post([FromBody] StudentVM model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                Student student = _mapper.Map<StudentVM, Student>(model);

                _repository.AddEntity(student);

                if (_repository.SaveAll())
                {
                    return Created($"api/orders/{student.ID}", student);
                }

            }
            catch (Exception)
            {
                return BadRequest("Server error");
            }
            return BadRequest("Failed to save student");
        }

        [HttpPut]
        public IActionResult PutStudent([FromBody] StudentVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Student student = _mapper.Map<StudentVM, Student>(model);

            if (!_repository.StudentExist(student.ID))
            {
                return NotFound();
            }

            _repository.UpdateEntity(student);
            if (_repository.SaveAll())
            {
                return Ok(student);
            }

            return NoContent();
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteStudent([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_repository.StudentExist(id))
            {
                return NotFound();
            }
            Student student = _repository.GetStudent(id);

            _repository.DeleteEntity(student);

            if (_repository.SaveAll())
            {
                return Ok(student);
            }

            return BadRequest("Course not deleted");
        }

        private bool CourseExists(int id)
        {
            return _repository.CourseExist(id);
        }


        [Route("[action]")]
        [HttpGet]
        public IActionResult Getgenders()
        {
            return Ok(_mapper.Map<IEnumerable<Gender>, IEnumerable<GenderVM>>(_repository.GetAllGenders()));
        }
    }
}
