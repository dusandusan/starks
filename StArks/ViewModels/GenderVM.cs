﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StArks.ViewModels
{
    public class GenderVM
    {
        public int ID { get; set; }
                
        public string Name { get; set; }
    }
}

