﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StArks.ViewModels
{
    public class MarkVM
    {
        public int ID { get; set; }

        [Required]
        public char MarkValue { get; set; }

        [Required]
        public int CourseID { get; set; }

        [Required]
        public int StudentID { get; set; }
    }
}
