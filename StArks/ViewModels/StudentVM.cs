﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StArks.ViewModels
{
    public class StudentVM
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(30)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(30)]
        public string Address { get; set; }

        [Required]
        [MaxLength(30)]
        public string City { get; set; }

        [Required]
        [MaxLength(30)]
        public string State { get; set; }

        [Required]
        public string DateOfBirth { get; set; }

        [Required]
        public int GenderID { get; set; }

        public ICollection<MarkVM> Marks { get; set; }

    }
}
