﻿using StArks.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StArks.Data
{
    public class StArksRepository : IStArksRepository
    {
        private readonly StArksContext _ctx;

        public StArksRepository(StArksContext ctx)
        {
            this._ctx = ctx;
        }


        public void AddEntity(object model)
        {
            _ctx.Add(model);
        }

        public void DeleteEntity(object model)
        {
            _ctx.Remove(model);
        }

        public void UpdateEntity(object model)
        {
            _ctx.Update(model);
        }

        public bool CourseExist(int id)
        {
            return _ctx.Courses.Any(e => e.ID == id);
        }

        public IEnumerable<Course> GetAllCourses()
        {
            return _ctx.Courses.Include(c => c.Marks);
        }
        public Course GetCourse(int id)
        {
            return _ctx.Courses.FirstOrDefault(x => x.ID == id);
        }

        public bool SaveAll()
        {
            return _ctx.SaveChanges() > 0;
        }

        public IEnumerable<Student> GetAllStudents()
        {
            return _ctx.Students.Include(s => s.Marks);
        }
        
        public Student GetStudent(int id)
        {
            return _ctx.Students.Include(s => s.Marks).FirstOrDefault(x => x.ID == id);
        }

        public bool StudentExist(int id)
        {
            return _ctx.Students.Any(e => e.ID == id);
        }

        public Mark GetMark(int id)
        {
            return _ctx.Marks.FirstOrDefault(x => x.ID == id);
        }

        public bool MarkExist(int id)
        {
            return _ctx.Marks.Any(e => e.ID == id);
        }

        public IEnumerable<Gender> GetAllGenders()
        {
            return _ctx.Gender.ToList();
        }
    }
}
