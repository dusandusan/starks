﻿using Microsoft.EntityFrameworkCore;
using StArks.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StArks.Data
{
    public class StArksContext : DbContext
    {
        public StArksContext(DbContextOptions<StArksContext> options) : base(options) { }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<Gender> Gender { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>()
            .HasMany(s => s.Marks)
            .WithOne()            
            .HasForeignKey(m => m.StudentID);

            modelBuilder.Entity<Course>()
            .HasMany(c => c.Marks)
            .WithOne()
            .HasForeignKey(m => m.CourseID);

            modelBuilder.Entity<Mark>()
                .HasKey(m => new { m.ID, m.CourseID, m.StudentID });
                //.Property(m => m.ID)
                //.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Gender>().HasData(
                new Gender() { ID = 1, Name= "Male"},
                new Gender() { ID = 2, Name= "Female"},
                new Gender() { ID = 3, Name= "Other"});

            modelBuilder.Entity<Course>().HasData(
                new Course() { ID = 1, Name = "ASP.Net core", Code = "c647" },
                new Course() { ID = 2, Name = "Angular", Code = "c546" },
                new Course() { ID = 3, Name = "SQL Server", Code = "c989" });

            modelBuilder.Entity<Student>().HasData(
                new Student { ID = 1, FirstName = "Dusan", LastName = "Jovanovic", Address = "Kneza Milosa 4", City = "Beograd", State = "Srbija", DateOfBirth = DateTime.Parse("1990-08-30"), GenderID = 1 },
                new Student { ID = 2, FirstName = "Sundjer", LastName = "Bob", Address = "nn ulica", City = "Koralovo", State = "Okean", DateOfBirth = DateTime.Parse("2000-01-01"), GenderID = 3 });

            modelBuilder.Entity<Mark>().HasData(
                new { ID = 1, MarkValue = 'b', StudentID = 1, CourseID = 1 },
                new { ID = 2, MarkValue = 'a', StudentID = 1, CourseID = 2 },
                new { ID = 3, MarkValue = 'd', StudentID = 1, CourseID = 3 },
                new { ID = 4, MarkValue = 'b', StudentID = 2, CourseID = 1 },
                new { ID = 5, MarkValue = 'c', StudentID = 2, CourseID = 2 },
                new { ID = 6, MarkValue = 'a', StudentID = 2, CourseID = 3 }
                );
        }

    }

}
