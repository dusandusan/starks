﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StArks.Data.Entities;
using StArks.ViewModels;

namespace StArks.Data
{
    public class StArksMapper: Profile
    {
        public StArksMapper()
        {
            CreateMap<Course, CourseVM>()
                .ReverseMap();

            CreateMap<Student, StudentVM>()
                .ReverseMap();

            CreateMap<Gender, GenderVM>()
                .ReverseMap();

            CreateMap<Mark, MarkVM>()
                .ReverseMap();
        }
    }
}
