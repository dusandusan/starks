﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StArks.Data.Entities
{
    public  class Course
    {
        
        public int ID { get; set; }

        [Required]
        [StringLength(4)]
        public string Code { get; set; }

        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }

        public ICollection<Mark> Marks { get; set; }

    }
}
