﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StArks.Data.Entities
{
    public class Mark
    {
        public int ID { get; set; }

        [Required]
        public char MarkValue { get; set; }

        [Required]
        public int CourseID { get; set; }

        [Required]
        public int StudentID { get; set; }
    }
}
