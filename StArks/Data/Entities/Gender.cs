﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StArks.Data.Entities
{
    public class Gender
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
