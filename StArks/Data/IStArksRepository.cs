﻿using System;
using System.Collections.Generic;
using StArks.Data.Entities;

namespace StArks.Data
{
    public interface IStArksRepository
    {

        void AddEntity(object model);

        void UpdateEntity(object model);

        void DeleteEntity(object model);

        IEnumerable<Course> GetAllCourses();

        Course GetCourse(int id);
        
        bool CourseExist(int id);
        
        IEnumerable<Student> GetAllStudents();

        Student GetStudent(int id);

        IEnumerable<Gender> GetAllGenders();

        bool StudentExist(int id);

        Mark GetMark(int id);

        bool MarkExist(int iD);

        bool SaveAll();

    }
}