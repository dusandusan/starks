﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StArks.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 4, nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Gender",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gender", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 30, nullable: false),
                    LastName = table.Column<string>(maxLength: 30, nullable: false),
                    Address = table.Column<string>(maxLength: 30, nullable: false),
                    City = table.Column<string>(maxLength: 30, nullable: false),
                    State = table.Column<string>(maxLength: 30, nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    GenderID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Marks",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    MarkValue = table.Column<string>(nullable: false),
                    CourseID = table.Column<int>(nullable: false),
                    StudentID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Marks", x => new { x.ID, x.CourseID, x.StudentID });
                    table.ForeignKey(
                        name: "FK_Marks_Courses_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Courses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Marks_Students_StudentID",
                        column: x => x.StudentID,
                        principalTable: "Students",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "ID", "Code", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "c647", null, "ASP.Net core" },
                    { 2, "c546", null, "Angular" },
                    { 3, "c989", null, "SQL Server" }
                });

            migrationBuilder.InsertData(
                table: "Gender",
                columns: new[] { "ID", "Name" },
                values: new object[,]
                {
                    { 1, "Male" },
                    { 2, "Female" },
                    { 3, "Other" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "ID", "Address", "City", "DateOfBirth", "FirstName", "GenderID", "LastName", "State" },
                values: new object[,]
                {
                    { 1, "Kneza Milosa 4", "Beograd", new DateTime(1990, 8, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dusan", 1, "Jovanovic", "Srbija" },
                    { 2, "nn ulica", "Koralovo", new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sundjer", 3, "Bob", "Okean" }
                });

            migrationBuilder.InsertData(
                table: "Marks",
                columns: new[] { "ID", "CourseID", "StudentID", "MarkValue" },
                values: new object[,]
                {
                    { 1, 1, 1, "b" },
                    { 2, 2, 1, "a" },
                    { 3, 3, 1, "d" },
                    { 4, 1, 2, "b" },
                    { 5, 2, 2, "c" },
                    { 6, 3, 2, "a" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Marks_CourseID",
                table: "Marks",
                column: "CourseID");

            migrationBuilder.CreateIndex(
                name: "IX_Marks_StudentID",
                table: "Marks",
                column: "StudentID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Gender");

            migrationBuilder.DropTable(
                name: "Marks");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "Students");
        }
    }
}
